﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileComparator
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Enter file extension to compare: ");
            String Extension = "." + Console.ReadLine();
            int lineNo = 0;

            string filePath1 = GetTargetFilePath(Extension);
            string filePath2 = GetTargetFilePath(Extension);

            using (StreamReader File1 = new StreamReader(filePath1), File2 = new StreamReader(filePath2))
            {
                while (!File1.EndOfStream && !File2.EndOfStream)
                {
                    lineNo++;
                    if (!File1.ReadLine().Equals(File2.ReadLine()))
                    {
                        Console.WriteLine("Files do not match at line {0}", lineNo.ToString());
                        System.Environment.Exit(0);
                    }
                }

                if (File1.EndOfStream && File2.EndOfStream)
                    Console.WriteLine("Files match");
                else
                    Console.WriteLine("Files do not match at line {0}", lineNo.ToString()) ;


            }
        }

        private static string GetTargetFilePath(string Extension)
        {
            string filePath = "";
            do
            {
                Console.WriteLine("Enter the file name you would like to compare:");
                filePath = Console.ReadLine();
                filePath += Extension;

                if (File.Exists(filePath) == false)
                    Console.WriteLine("The file cannot be found");

            } while (filePath.EndsWith(Extension) == false || File.Exists(filePath) == false);

            return filePath;
        }
    
    }
}
